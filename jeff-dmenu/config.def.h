// vim: filetype=c
// jeff-dmenu/config.def.h

static int topbar =			0;
static const char *prompt =		NULL;
static unsigned int lines =		0;
static unsigned int lineheight =	24;
static unsigned int min_lineheight =	8;
static const char worddelimiters[] =	" ";

static const char *fonts[] = {
	"Cascadia Code:size=14"
};

static const char *colors[SchemeLast][2] = {
	[SchemeNorm]	= { "#bbbbbb", "#222222" },
	[SchemeSel]	= { "#eeeeee", "#005577" },
	[SchemeOut]	= { "#000000", "#00ffff" },
};
