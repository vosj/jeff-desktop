#!/bin/dash

os="Void Linux"
host=$(hostname)

display_os() {
	xsetroot -name " $os "
	sleep 1
}

display_host() {
	xsetroot -name " $host "
	sleep 1
}

display_time() {
	time=$(date +"%I:%M:%S %p")
	xsetroot -name " $time "
	sleep 1
}

display_date() {
	date=$(date +"%a %m/%d/%Y")
	xsetroot -name " $date "
	sleep 1
}

while :; do
	pulse=$(date +%S | sed 's/^0//')
	if [ "$pulse" -le 10 ] && [ "$pulse" -ge 1 ]; then
		display_time
	elif [ "$pulse" -le 40 ] && [ "$pulse" -ge 31 ]; then
		display_time
	elif [ "$pulse" -le 20 ] && [ "$pulse" -ge 11 ]; then
		display_date
	elif [ "$pulse" -le 50 ] && [ "$pulse" -ge 41 ]; then
		display_date
	elif [ "$pulse" -le 25 ] && [ "$pulse" -ge 21 ]; then
		display_os
	elif [ "$pulse" -le 55 ] && [ "$pulse" -ge 51 ]; then
		display_os
	elif [ "$pulse" -le 30 ] && [ "$pulse" -ge 26 ]; then
		display_host
	else
		display_host
	fi
done
